# Starting the Proxy
```bash
docker-compose up -d
```

# Making other containers accessible
Give your Web-Service container the env var `VIRTUAL_HOST` and put it in the same network as the proxy, for example:
```yaml
services:
    whoami:
      image: jwilder/whoami
      environment:
        - VIRTUAL_HOST=whoami.local
      networks:
        - proxy_docker-proxy
        - default

networks:
  proxy_docker-proxy:
    external: true
```

# Remarks
If you use this for your testing environment, your machine still needs to know that the host is reachable at
`localhost`, so for the example above you need to add `127.0.0.1    whoami.local` to `/etc/hosts`
